#!/usr/local/bin/python3

import matplotlib as mpl
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from sklearn import svm, neighbors, neural_network

from collections import Counter

from ml_util import *
from util import *
import joblib

class1 = 'ttb'
class2 = 'delta'
class3 = 'tally'
STRATEGY_TO_LABEL_MAPPING = {class1: 0, class2: 1, class3: 2}
NUM_QUESTIONS = 40
TEST_NUM_QUESTIONS = 120

args = parse_args_run_ml_script()
training_data_dir = args.training_data_dir
raw_data_dir = args.raw_data_dir

run_on_test_data = args.run_on_test_data
if run_on_test_data:
	test_data_dir = args.test_data_dir
	test_data_file = args.test_data_file
	test_data_user_answers = read_user_ans_files(test_data_dir, two_levels_deep=False, answers_str_at_start=False)
	test_stimuli_rows = cache_data_file('/Users/cyu/jfdata/dropbox/ExperimentIII_ttb_delta_tally.csv')

stimuli_rows = cache_data_file('/Users/cyu/jfdata/dropbox/ExperimentII_ttb_delta_tally.csv')

user_answers = read_user_ans_files([raw_data_dir], two_levels_deep=True)

# prolific_ttb_data_rows = cache_data_file(test_ttb_data_file)
# prolific_tally_data_rows = cache_data_file(test_tally_data_file)
# ordering = get_question_ordering('/Users/cyu/prolific_data/20200202/participants.csv')
# ordering = get_question_ordering('/Users/cyu/2020data/prolific_data/20200224/usable/participants.csv')

#  TRAINING  #

# min_max_scaler = preprocessing.MinMaxScaler()
# minmax_durations = min_max_scaler.fit_transform(data[:, [i for i in range(11)]])
# data = np.concatenate((minmax_durations, data[:, [i for i in range(11, 21)]]), axis=1)
# normalized_durations = preprocessing.normalize(data[:, [i for i in range(11)]], axis=0)
# data = np.concatenate((normalized_durations, data[:, [i for i in range(11, 21)]]), axis=1)

# adb_dt_count_points(training_data_dir, DIM)

# data, labels = gen_data(21, 'stats_combined.csv', 4)
# data, labels = gen_data(200, 'samples_combined.csv', 2)
# random_order = np.random.permutation(data.shape[0])
# labels = np.asarray(labels)
# labels = labels[random_order]
# data = data[random_order]
# data = preprocessing.normalize(data, axis=0)

# tuned_parameters = [
#   {'solver': ['lbfgs', 'sgd', 'adam'], 'learning_rate': ['constant', 'invscaling', 'adaptive']}
#  ]
# clf = model_selection.GridSearchCV(neural_network.MLPClassifier(), tuned_parameters, cv=10)

# clf = naive_bayes.BernoulliNB()
# two_labels_five_classes(clf)
# train_predict_and_print_metrics(clf, X_train, X_test, y_train, y_test, False)

# clf = naive_bayes.GaussianNB()
# two_labels_five_classes(clf)
# train_predict_and_print_metrics(clf, X_train, X_test, y_train, y_test, False)

# clf = naive_bayes.MultinomialNB()
# two_labels_five_classes(clf)
# train_predict_and_print_metrics(clf, X_train, X_test, y_train, y_test, False)


CLASSIFIER_MODEL_FILE = os.path.join(training_data_dir, 'classifier.pkl')
if not os.path.isfile(CLASSIFIER_MODEL_FILE):
	data, labels, uuid_to_answers, uuid_to_datapoints = gen_data(num_features=26,
	                                                             file_name=os.path.join(training_data_dir, 'stats_combined.csv'),
	                                                             leading_features_to_discard=4,
	                                                             stimuli_rows=stimuli_rows,
	                                                             user_answers=user_answers,
	                                                             strategy_to_label_mapping=STRATEGY_TO_LABEL_MAPPING,
	                                                             class1_name=class1,
	                                                             class2_name=class2,
	                                                             class3_name=class3,
	                                                             num_questions=NUM_QUESTIONS,
	                                                             add_hint=True)
	data = preprocessing.normalize(data, norm='l2', axis=1)

# delta_tally_user_answers = read_user_ans_files(['/Users/cyu/jfdata/dropbox/2019/strategyRecovery/1115_delta',
#                                                  '/Users/cyu/jfdata/dropbox/2019/strategyRecovery/1118_delta'])
# delta_tally_data, delta_tally_labels, delta_tally_uuid_to_answers, delta_tally_uuid_to_datapoints = gen_data(num_features=25,
#                                                              file_name=os.path.join('/Users/cyu/jfdata/dropbox/2019/strategyRecovery/1115_1118_stats_combined.csv'),
#                                                              leading_features_to_discard=4,
#                                                              ttb_data_rows=ttb_data_rows,
#                                                              delta_data_rows=delta_data_rows,
#                                                              tally_data_rows=tally_data_rows,
#                                                              user_answers=delta_tally_user_answers,
#                                                              num_questions=NUM_QUESTIONS,
#                                                              add_hint=True)

# prolific_ttb_user_answers = read_user_ans_files(['/Users/cyu/2020data/prolific_data/20200224'])
# prolific_ttb_data, prolific_ttb_labels, prolific_ttb_uuid_to_answers, prolific_ttb_uuid_to_datapoints = gen_data(
# 	num_features=25,
# 	file_name=os.path.join('/Users/cyu/2020data/prolific_data/20200224/usable/stats_combined.csv'),
# 	leading_features_to_discard=4,
# 	ttb_data_rows=prolific_ttb_data_rows,
# 	delta_data_rows=[],
# 	tally_data_rows=[],
# 	user_answers=prolific_ttb_user_answers,
# 	num_questions=NUM_QUESTIONS,
# 	question_ordering=ordering,
# 	add_hint=True)

	tuned_parameters = {'max_depth': [50, 100, 150],
	                    'min_samples_split': [2, 5, 10],
	                    'n_estimators': [20, 50, 80]
	                    }
	RF = ensemble.RandomForestClassifier()
	clf = model_selection.GridSearchCV(RF, param_grid=tuned_parameters, cv=10)

	# tuned_parameters = {"base_estimator__criterion": ["gini", "entropy"],
	# 			  "base_estimator__splitter": ["best", "random"],
	# 			  "n_estimators": [20, 50, 80]
	# 			 }
	# ABC = ensemble.AdaBoostClassifier(base_estimator=tree.DecisionTreeClassifier())
	# clf = model_selection.GridSearchCV(ABC, param_grid=tuned_parameters, cv=10)

	# http://stackoverflow.com/questions/17335738/results-on-libsvm-favors-only-one-class-from-two-classes
	# tuned_parameters = [
	#   {'C': [0.01], 'kernel': ['linear']},
	#   {'C': [0.01], 'gamma': [0.001, 0.0001], 'kernel': ['rbf']}
	# ]
	# clf = model_selection.GridSearchCV(svm.SVC(), tuned_parameters, cv=10)

	# kd tree not suitable for high dimensional data
	# tuned_parameters = [
	#   {'n_neighbors': [10, 20, 30], 'weights': ['distance', 'uniform'], 'algorithm': ['ball_tree']}
	#  ]
	# clf = model_selection.GridSearchCV(neighbors.KNeighborsClassifier(), tuned_parameters, cv=10)

	# tuned_parameters = [{'min_samples_split': [5, 10, 20], 'min_samples_leaf': [5, 10, 20], 'max_depth': [10, 20]}]
	# clf = model_selection.GridSearchCV(tree.DecisionTreeClassifier(), tuned_parameters, cv=10)

	# clf = neural_network.MLPClassifier(hidden_layer_sizes=(53,))

# X_train, X_test, y_train, y_test = partition_data(data, labels)
# X_train, X_test, y_train, y_test = partition_full_heldout(uuid_to_datapoints, uuid_to_answers,
#                                                           test_ttb_num=0, test_tally_num=0, test_delta_num=0)

	X_train, y_train, X_test, y_test = partition_uuid_to_datapoints(uuid_to_datapoints=uuid_to_datapoints,
	                                                                class1_test_size=8, class2_test_size=8, class3_test_size=8,
	                                                                class1_name=class1, class2_name=class2, class3_name=class3,
	                                                                strategy_to_label_mapping=STRATEGY_TO_LABEL_MAPPING)

# prolific_ttb_X_train, prolific_ttb_X_test, prolific_ttb_y_train, prolific_ttb_y_test = partition_full_heldout(
# 	prolific_ttb_uuid_to_datapoints, prolific_ttb_uuid_to_answers,
# 	test_ttb_num=5, test_tally_num=0, test_delta_num=0,
# 	train_delta_num=0, train_ttb_num=0, train_tally_num=0)
#
# X_train = np.append(X_train, prolific_ttb_X_train, 0)
# y_train = y_train + prolific_ttb_y_train
# X_test = np.append(X_test, prolific_ttb_X_test, 0)
# y_test = y_test + prolific_ttb_y_test

# X_train, X_test, y_train, y_test = model_selection.train_test_split(X_train, y_train, test_size=0.20, random_state=42)

	print('X_train shape:', X_train.shape)
	print('X_test shape:', X_test.shape)
	print('y_train len:', len(y_train))
	print('y_test len:', len(y_test))

	classes, class_y = split_test_dataset_into_five_classes(X_test, y_test, stimuli_rows,
	                                                        num_features=26, num_questions=NUM_QUESTIONS)

# fig = plt.figure(1, figsize=(4, 3))
# plt.clf()
# ax = Axes3D(fig, rect=(0, 0, .95, 1), elev=48, azim=134)
#
# plt.cla()
# pca = decomposition.PCA(n_components=3)
# pca.fit(X_train)
# X = pca.transform(X_train)
# condition = abs(X) < 20
# X = X[condition.all(axis=1)]
#
# y_train = np.asarray(y_train).astype(int)
# print(y_train.shape)
# y_train = y_train[condition.all(axis=1)]
# print(y_train.shape)
# base_colors = ['green', 'orange', 'blue']
# colors = []
# for elem in y_train:
# 	colors.append(base_colors[elem])
# ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=colors)
#
# ax.w_xaxis.set_ticklabels([])
# ax.w_yaxis.set_ticklabels([])
# ax.w_zaxis.set_ticklabels([])
# plt.show()

	print('Overall result on whole test dataset:')
	_, _, clf, predicted, order = train_predict_and_print_metrics(clf, X_train, X_test, y_train, y_test,
	                                                              num_questions=NUM_QUESTIONS,
	                                                              tuned_params=True)
	joblib.dump(clf, CLASSIFIER_MODEL_FILE)

	print('Results on sub-test datasets partitioned by discriminating cue:')
	for i in range(5):
		_, _, clf, predicted, order = predict_and_print_metrics(clf, classes[i], class_y[i],
		                                                        num_questions=NUM_QUESTIONS)

# probs = clf.predict_proba(X_test)
# for idx in range(np.shape(probs)[0]):
# 	val = probs[idx]
# 	if max(val) <= AMBIGUITY_THRESHOLD_PROB:
# 		predicted[idx] = 3
#
# for elem in y_test:
# 	count = 0
# 	idx = 0
# 	while idx < len(y_test):
# 		if idx % 120 == 0:
# 			count = 0
# 		if predicted[idx] == y_test[idx]:
# 			count += 1
# 		idx += 1

#  TESTING  #

if run_on_test_data:
	# If run on test/unlabeled data, then train the model on all available labeled data
	CLASSIFIER_MODEL_FILE = os.path.join(training_data_dir, 'classifier.pkl')
	clf = joblib.load(CLASSIFIER_MODEL_FILE)
	test_data = gen_test_data(num_features=26,
	         test_data_file=test_data_file,
	         leading_features_to_discard=4,
	         test_stimuli_rows=test_stimuli_rows,
	         user_answers=test_data_user_answers,
	         num_questions=TEST_NUM_QUESTIONS,
	         add_hint=True)
	num_test_subjects = np.shape(test_data)[0] // TEST_NUM_QUESTIONS

	'''
	blocked_accuracies = []
	for i in range(num_test_subjects):
		blocked_accuracies.append([])
		for j in range(12):
			blocked_accuracies[-1].append(sum(test_data[j*10 + i*NUM_QUESTIONS:(j+1)*10 + i*NUM_QUESTIONS, -3]) / 10)

	to_be_deleted = []
	for i in range(len(blocked_accuracies)):
		for j in range(6, 12):
			if blocked_accuracies[i][j] <= .6:
				to_be_deleted = to_be_deleted + [k for k in range(i*120, (i+1)*120)]
	test_data = np.delete(test_data, to_be_deleted, axis=0)
	num_test_subjects = np.shape(test_data)[0] // TEST_NUM_QUESTIONS
	print(test_data.shape)
	print(blocked_accuracies)

	x_axis = [i for i in range(12)]
	for i in range(num_test_subjects):
		plt.plot(x_axis, blocked_accuracies[i])
	plt.show()
	'''

	predicted = clf.predict(test_data)

	to_be_deleted = []
	for i in range(num_test_subjects):
		if len([x for x in predicted[i*120 + 80:(i+1)*120] if x != 0]) > 20:
			to_be_deleted = to_be_deleted + [k for k in range(i*120, (i+1)*120)]
	test_data = np.delete(test_data, to_be_deleted, axis=0)
	predicted = [predicted[i] for i in range(len(predicted)) if i not in to_be_deleted]
	print(test_data.shape)
	print(len(predicted))
	num_test_subjects = np.shape(test_data)[0] // TEST_NUM_QUESTIONS

	print('Number of test subjects: {}'.format(str(num_test_subjects)))
	print('Number of predictions made: {}'.format(str(np.shape(test_data)[0])))
	print('TTB: %.4f' % (len([x for x in predicted if x == 0]) / len(predicted)))
	print('Delta: %.4f' % (len([x for x in predicted if x == 1]) / len(predicted)))
	print('Tally: %.4f' % (len([x for x in predicted if x == 2]) / len(predicted)))

	probs = clf.predict_proba(test_data)
	np.savetxt("probs.csv", probs, delimiter=",")
	ambiguous = 0
	for idx in range(np.shape(probs)[0]):
		val = probs[idx]
		if max(val) <= AMBIGUITY_THRESHOLD_PROB:
			predicted[idx] = 3
			ambiguous += 1
	print('Ambiguity threshold probability: {}'.format(str(AMBIGUITY_THRESHOLD_PROB)))
	print('Number of ambiguous predictions: {}'.format(str(ambiguous)))
	print('Percentage of ambiguous predictions: %.4f' % (ambiguous / np.shape(test_data)[0]))

	ttb_conf = np.array([sum(probs[i * TEST_NUM_QUESTIONS:i * TEST_NUM_QUESTIONS + TEST_NUM_QUESTIONS, 0]) /
	                     TEST_NUM_QUESTIONS for i in range(num_test_subjects)])
	delta_conf = np.array([sum(probs[i * TEST_NUM_QUESTIONS:i * TEST_NUM_QUESTIONS + TEST_NUM_QUESTIONS, 1]) /
	                       TEST_NUM_QUESTIONS for i in range(num_test_subjects)])
	tally_conf = np.array([1 - ttb_conf[i] - delta_conf[i] for i in range(num_test_subjects)])

	order = np.array(sorted(range(len(ttb_conf)), key=lambda k: ttb_conf[k]))
	'''
	sorted_ttb_conf = ttb_conf[order]
	sorted_delta_conf = delta_conf[order]
	sorted_tally_conf = tally_conf[order]
	'''

	# ind = np.arange(num_test_subjects)
	# sorted_stacked_bottom = [sorted_delta_conf[i] + sorted_ttb_conf[i] for i in range(num_test_subjects)]
	# p1 = plt.bar(ind, sorted_tally_conf, bottom=sorted_stacked_bottom)
	# p2 = plt.bar(ind, sorted_delta_conf, bottom=sorted_ttb_conf)
	# p3 = plt.bar(ind, sorted_ttb_conf)
	# plt.title('Predicted Strategy Confidence Sorted by TTB')
	# plt.ylabel('Confidence Values')
	# plt.xlabel('Test Subjects')
	# plt.legend((p1[0], p2[0], p3[0]), ('Tally', 'Delta', 'TTB'))
	# print('Default color options: ', plt.rcParams['axes.prop_cycle'].by_key()['color'])
	# plt.show()
	#
	# stacked_bottom = [delta_conf[i] + ttb_conf[i] for i in range(num_test_subjects)]
	# p1 = plt.bar(ind, tally_conf, bottom=stacked_bottom)
	# p2 = plt.bar(ind, delta_conf, bottom=ttb_conf)
	# p3 = plt.bar(ind, ttb_conf)
	# plt.title('Predicted Strategy Confidence')
	# plt.ylabel('Confidence Values')
	# plt.xlabel('Test Subjects')
	# plt.legend((p1[0], p2[0], p3[0]), ('Tally', 'Delta', 'TTB'))
	# plt.show()

	# vals = np.asarray(predicted).reshape(TEST_NUM_QUESTIONS, int(len(predicted) / TEST_NUM_QUESTIONS))
	# cmap = mpl.colors.ListedColormap(['#1f77b4', '#ff7f0e', '#2ca02c', 'white'])
	# img = plt.imshow(vals, interpolation='nearest', cmap=cmap)
	# plt.title('Test Subject Strategy Predictions')
	# plt.xlabel('Test Subject')
	# plt.ylabel('Decision Trial')
	# clb = plt.colorbar(img, cmap=cmap, ticks=[0, 1, 2, 3])
	# clb.ax.set_yticklabels(['TTB', 'Delta', 'Tally', 'Ambiguous'])
	# plt.show()

	BLOCK_SIZE = 1
	idx = 0
	agg_predicted = []
	while idx < len(predicted) // BLOCK_SIZE:
		curr = predicted[idx * BLOCK_SIZE:(idx + 1) * BLOCK_SIZE]
		occurrence_count = Counter(curr)
		agg_predicted.append(occurrence_count.most_common(1)[0][0])
		idx += 1
	print('aggregated prediction length: ', len(agg_predicted))

	vals = np.asarray(agg_predicted).reshape(len(agg_predicted) // (TEST_NUM_QUESTIONS // BLOCK_SIZE), TEST_NUM_QUESTIONS
	                                         // BLOCK_SIZE).astype(int)
	np.savetxt('/Users/cyu/Documents/predict.csv', vals, delimiter=',')
	sorted_vals = vals[tuple(order), :]
	cmap = mpl.colors.ListedColormap(['#1f77b4', '#ff7f0e', '#2ca02c'])
	img = plt.imshow(sorted_vals, interpolation='nearest', cmap=cmap)
	plt.title('Test Subject Strategy Predictions')
	plt.ylabel('Test Subject')
	plt.xlabel('Decision Trial')
	# clb = plt.colorbar(img, cmap=cmap, ticks=[0, 1, 2, 3])
	# clb.ax.set_yticklabels(['TTB', 'Delta', 'Tally', 'Ambiguous'])
	clb = plt.colorbar(img, cmap=cmap, ticks=[0, 1, 2])
	clb.ax.set_yticklabels(['TTB', 'Delta', 'Tally'])
	plt.show()
