#!/usr/local/bin/python3
# ./merge_files.py ../result
import csv

from util import *

args = parse_args_compute_stats_script()
input_dir = args.input_dir
computing_for_test_data = args.test_data

# if not computing_for_test_data:
# 	valid_users = get_users_in_file(input_dir, 'valid_users.txt')

STATS_COMBINED_FILENAME = os.path.join(input_dir, 'stats_combined.csv')
if os.path.isfile(STATS_COMBINED_FILENAME):
	os.remove(STATS_COMBINED_FILENAME)

stats_files = [filename for filename in os.listdir(input_dir) if filename.startswith('stats') and filename.endswith('.csv')]

NUM_QUESTIONS = 60


def combine_files(file_prefix, combined_filename, files):
	with open(combined_filename, 'w') as output:
		writer = csv.writer(output, delimiter=',')
		for single_file in files:
			prefix_len = len(file_prefix)
			curr_uuid = single_file[prefix_len:(UUID_LENGTH + prefix_len)]
			# if not computing_for_test_data and curr_uuid not in valid_users:
			# 	continue
			with open(os.path.join(input_dir, single_file), 'r') as csv_file:
				reader = csv.reader(csv_file, delimiter=',')
				count = 0
				rows = []
				for row in reader:
					count += 1
					rows.append(row)
				if count > NUM_QUESTIONS:
					print('More than {} lines in file {}, with {} lines.'.format(NUM_QUESTIONS, single_file, str(count)))
					continue
				writer.writerows(rows)


combine_files('stats_', STATS_COMBINED_FILENAME, stats_files)
