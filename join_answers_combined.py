#! /usr/local/bin/python3
import csv

from ml_util import get_question_ordering
from util import arg_parser

arg_parser.add_argument('answers_file')
arg_parser.add_argument('combined_file')
arg_parser.add_argument('joined_file')
args = arg_parser.parse_args()
answers_file = args.answers_file
combined_file = args.combined_file
joined_file = args.joined_file

ordering = get_question_ordering('/Users/cyu/2020data/gdata/participants.csv')

answers_dict = dict()
with open(answers_file, 'r') as csv_file:
	reader = csv.reader(csv_file, delimiter=',')
	next(reader)
	for row in reader:
		q_num = ordering[row[0]][int(row[1])]
		answers_dict[(row[0], q_num)] = (row[2], row[3])

combined_dict = dict()
with open(combined_file, 'r') as csv_file:
	reader = csv.reader(csv_file, delimiter=',')
	for row in reader:
		combined_dict[(row[1], int(row[2]))] = (row[0], row[4])

with open(joined_file, 'w') as csvfile:
	writer = csv.writer(csvfile)
	for key in combined_dict:
		if key not in answers_dict:
			continue
		writer.writerow(list(key) + list(combined_dict[key]) + list(answers_dict[key]))
