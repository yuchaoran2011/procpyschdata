#!/usr/local/bin/python3
# ./sample_coords.py ../result

import csv

from dateutil import parser
from datetime import timedelta
import ntpath
import sys
from util import *

MIN_SAMPLES = 10
SAMPLING_INTERVAL = DURATION_PER_QUESTION_IN_SECS / SAMPLES_PER_QUESTION

input_dir = sys.argv[1]
valid_users = get_users_in_file(input_dir, 'valid_users.txt')

files = [filename for filename in os.listdir(input_dir) if not filename.startswith('stats') and
		 not filename.startswith('samples') and not 'combined' in filename and filename.endswith('.csv')]


def produce_output_row(first_cols, rows):
	x_coords = [rows[0][3]]
	y_coords = [rows[0][4]]

	index = 0
	base_time = parser.parse(rows[0][2])
	count = 1
	while index < len(rows):
		curr_time = parser.parse(rows[index][2])
		if timedelta.total_seconds(curr_time - base_time) > SAMPLING_INTERVAL and count < SAMPLES_PER_QUESTION:
			x_coords.append(rows[index][3])
			y_coords.append(rows[index][4])
			base_time = curr_time
			count += 1
		index += 1

	for _ in range(SAMPLES_PER_QUESTION - count):
		x_coords.append(0)
		y_coords.append(0)
	return first_cols + x_coords + y_coords


for filename in files:
	with open(filename, 'r') as csv_file:

		curr_file = 'samples_' + ntpath.basename(filename)
		if os.path.exists(curr_file):
			continue
		reader = csv.reader(csv_file, delimiter=',')
		prev_question = ''

		with open(curr_file, 'w') as output:
			writer = csv.writer(output, delimiter=',')

			for row in reader:
				curr_row = row
				try:
					float(row[3])
				except ValueError:
					curr_row = [row[0], row[4], row[3], row[1], row[2]]

				if prev_question == '':
					prev_question = curr_row[1]
					lines = [curr_row]
				elif curr_row[1] != prev_question:
					prev_question = curr_row[1]
					game_id = int(curr_row[1]) - 1
					if len(lines) > MIN_SAMPLES and curr_row[0] in valid_users:
						writer.writerow(produce_output_row([curr_row[0], game_id], lines))
					lines = []
				else:
					lines.append(curr_row)

			# Write samples for the last question
			if len(lines) > MIN_SAMPLES:
				if len(lines) >= SAMPLES_PER_QUESTION and curr_row[0] in valid_users:
					writer.writerow(produce_output_row([curr_row[0], game_id], lines))
