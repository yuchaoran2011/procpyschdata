#! /usr/local/bin/python3
from collections import Counter

from mpl_toolkits.mplot3d import Axes3D
from sklearn import *

from ml_util import *
from util import AMBIGUITY_THRESHOLD_PROB
import matplotlib as mpl
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

class1 = 'ttb'
class2 = 'wadd'
class3 = 'tally'
STRATEGY_TO_LABEL_MAPPING = {class1: 0, class2: 1, class3: 2}
NUM_QUESTIONS = 60

data_rows = cache_data_file('/Users/cyu/Documents/g_data.csv')

user_answers = read_user_ans_files('/Users/cyu/jfdata/2020data/gdata', two_levels_deep=False, answers_str_at_start=True)

ordering = get_question_ordering('/Users/cyu/jfdata/2020data/gdata/participants.csv')

_, _, _, uuid_to_datapoints = gen_data(num_features=21,
                                       file_name=os.path.join('/Users/cyu/Documents/stats_combined_g.csv'),
                                       leading_features_to_discard=4,
                                       stimuli_rows=data_rows,
                                       user_answers=user_answers,
                                       num_questions=NUM_QUESTIONS,
                                       class1_name=class1,
                                       class2_name=class2,
                                       class3_name=class3,
                                       strategy_to_label_mapping=STRATEGY_TO_LABEL_MAPPING,
                                       question_ordering=ordering,
                                       add_hint=False)

X_train, y_train, X_test, y_test = partition_uuid_to_datapoints(uuid_to_datapoints=uuid_to_datapoints,
                                                                class1_test_size=5, class2_test_size=5, class3_test_size=5,
                                                                class1_name=class1, class2_name=class2, class3_name=class3,
                                                                strategy_to_label_mapping=STRATEGY_TO_LABEL_MAPPING)
                                                                # num_questions=NUM_QUESTIONS)

# pca = decomposition.PCA(n_components=2)
# pca.fit(X_train)
# X_train = pca.transform(X_train)
# X_test = pca.transform(X_test)
#
# cdict = {0: 'red', 1: 'blue', 2: 'black'}
# _, ax = plt.subplots()
# group = np.array(y_train)
# for g in np.unique(group):
#     ix = np.where(group == g)
#     ax.scatter(X_train[ix, 0],  X_train[ix, 1], c = cdict[g], label = g, s = 10)
# ax.legend()
# plt.show()

tuned_parameters = {'max_depth': [50, 100, 150],
                    'min_samples_split': [2, 5, 10],
                    'n_estimators': [20, 50, 80]
                    }
model = ensemble.RandomForestClassifier()

# tuned_parameters = [
#   {'C': [0.01], 'kernel': ['linear']},
#   {'C': [0.01], 'gamma': [0.001, 0.0001], 'kernel': ['rbf']}
# ]
# model = svm.SVC()

# tuned_parameters = [
#   {'n_neighbors': [10, 20, 30], 'weights': ['distance', 'uniform'], 'algorithm': ['ball_tree']}
#  ]
# model = neighbors.KNeighborsClassifier()

# tuned_parameters = {"base_estimator__criterion": ["gini", "entropy"],
# 			  "base_estimator__splitter": ["best", "random"],
# 			  "n_estimators": [20, 50, 80]
# 			 }
# model = ensemble.AdaBoostClassifier(base_estimator=tree.DecisionTreeClassifier())

# tuned_parameters = [{'min_samples_split': [5, 10, 20], 'min_samples_leaf': [5, 10, 20], 'max_depth': [10, 20]}]
# model = tree.DecisionTreeClassifier()

clf = model_selection.GridSearchCV(model, param_grid=tuned_parameters, cv=10)

print('X_train shape:', X_train.shape)
print('X_test shape:', X_test.shape)
print('y_train len:', len(y_train))
print('y_test len:', len(y_test))

_, _, clf, predicted, order = train_predict_and_print_metrics(clf, X_train, X_test, y_train, y_test, num_questions=NUM_QUESTIONS, tuned_params=False)

probs = clf.predict_proba(X_test)
for idx in range(np.shape(probs)[0]):
    val = probs[idx]
    if max(val) <= AMBIGUITY_THRESHOLD_PROB:
        predicted[idx] = 3


BLOCK_SIZE = 1
idx = 0
agg_predicted = []
while idx < len(predicted) // BLOCK_SIZE:
    curr = predicted[idx * BLOCK_SIZE:(idx + 1) * BLOCK_SIZE]
    occurrence_count = Counter(curr)
    agg_predicted.append(occurrence_count.most_common(1)[0][0])
    idx += 1
print('aggregated prediction length: ', len(agg_predicted))

vals = np.asarray(agg_predicted).reshape(len(agg_predicted) // (60 // BLOCK_SIZE), 60 // BLOCK_SIZE).astype(int)

# Flip to make ordering consistent with colorbar below
# sorted_vals = np.flip(vals[tuple(order), :])
sorted_vals = vals[tuple(order), :]

fig = plt.figure()
cmap = mpl.colors.ListedColormap(['#1f77b4', '#ff7f0e', '#2ca02c'])
img = plt.pcolor(sorted_vals, edgecolors='black', linewidths=4, cmap=cmap)
plt.title('Test Subject Strategy Predictions')
plt.ylabel('Test Subject')
plt.xlabel('Decision Trial')
ax = plt.gca()
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
clb = plt.colorbar(img, cax=cax, ticks=[0, 1, 2])
clb.ax.set_yticklabels(['TTB', 'WADD', 'Tally'])
plt.show()
