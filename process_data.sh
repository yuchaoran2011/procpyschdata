#!/usr/bin/env bash
# Example usage: ./process_data.sh movements.csv answers.csv ../output ../output_usable 80

movements_file="$1"
answers_files="$2"
folder="$3" # The folder that contains coordinate files for all users
output_folder="$4"
correctness="$5"

echo 'Processing raw files to separate users...'
./process_csv.py ${movements_file} ${folder}
echo 'Filtering out invalid users...'
./filter_users.py ${answers_files} ${folder} ${output_folder} --correctness ${correctness}

echo 'Computing statistics...'
./compute_stats.py ${output_folder}
echo 'Down-sampling movement coordinates...'
./sample_coords.py ${output_folder}
echo 'Merging generated files...'
./merge_files.py ${output_folder}
echo 'Cleaning up...'
find ${output_folder} -size  0 -print0 | xargs -0 rm
