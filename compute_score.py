#!/usr/local/bin/python3
# ./compute_score.py input_dir

import csv

from util import *

args = parse_args_compute_scores_script()
input_dir = args.input_dir
num_questions = int(args.num_questions)

scores = dict()

ans_files = [filename for filename in os.listdir(input_dir) if filename.endswith('_answers.csv')]

for ans_file in ans_files:
	line_num = 0
	count = 0
	uuid = ''
	with open(os.path.join(input_dir, ans_file), 'r') as csv_file:
		reader = csv.reader(csv_file, delimiter=',')
		for row in reader:
			uuid = row[0]
			count += 1 if row[2] == row[3] else 0
			if (line_num + 1) % num_questions == 0:
				print(uuid, count / num_questions)
				count = 0
			line_num += 1
		if count != 0:
			print(uuid, count / num_questions)
			count = 0
