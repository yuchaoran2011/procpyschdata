#!/usr/local/bin/python3
# ./compute_stats.py ../result

# Compute the following stats for boxes
# 1. Duration staying at each box
# 2. Total number of opened boxes for each question

# Boxes are all 50 * 50
# Coordinates:
# Left column upper left corners:
# x_coord: 310
# y_coord: 50, 150, 250, 350, 450

# right column upper left corners:
# x_coord: 695
# y_coord: 50, 150, 250, 350, 450

import collections
import csv

from dateutil import parser
from datetime import timedelta
import ntpath

from util import *

# BOX_Y_COORDS = [68, 168, 268, 368, 468]
# BOX_X_COORDS = [320, 707.5]

BOX_Y_COORDS = [70, 170, 270, 370]
BOX_X_COORDS = [320, 707.5]

BOX_HEIGHT = 50
BOX_WIDTH = 50
EMPTY_BOX_DURATIONS = {
	0: 0.0,
	1: 0.0,
	2: 0.0,
	3: 0.0,
	4: 0.0,
	5: 0.0,
	6: 0.0,
	7: 0.0
}

args = parse_args_compute_stats_script()
input_dir = args.input_dir
computing_for_test_data = args.test_data

uuid_strategy_map = dict()
if not computing_for_test_data:
	# valid_ttb_users = get_users_in_file(input_dir, 'valid_ttb_users.txt')
	# valid_tally_users = get_users_in_file(input_dir, 'valid_tally_users.txt')
	# valid_delta_users = get_users_in_file(input_dir, 'valid_delta_users.txt')
	# valid_users = get_users_in_file(input_dir, 'valid_users.txt')
	# tag_to_valid_users = {'ttb': valid_ttb_users, 'tally': valid_tally_users, 'delta': valid_delta_users}
	with open(os.path.join(input_dir, 'participants.csv'), 'r') as csv_file:
		reader = csv.reader(csv_file, delimiter=',')
		next(reader)
		for row in reader:
			# For newer data, change below to uuid_strategy_map[row[0]] = row[1]
			uuid_strategy_map[row[0]] = row[1]


def get_num_opened_boxes(opened_boxes):
	return len(opened_boxes)


def set_order_unopened(opened_boxes):
	return opened_boxes + [0 for _ in range(len(EMPTY_BOX_DURATIONS.keys()) - len(opened_boxes))]


def row_when_decision_made(orders):
	if len(orders) == 0:
		return -1
	last_box = orders[len(orders) - 1] - 1
	if last_box % 2 == 0:
		return int(last_box / 2)
	else:
		return int((last_box - 1) / 2)


# Function to determine which box is opened
def opened_box_number(x_coord, y_coord):
	if BOX_X_COORDS[0] <= x_coord <= BOX_X_COORDS[0] + BOX_WIDTH:
		for i in range(len(BOX_Y_COORDS)):
			if BOX_Y_COORDS[i] <= y_coord <= BOX_Y_COORDS[i] + BOX_HEIGHT:
				return i
	elif BOX_X_COORDS[1] <= x_coord <= BOX_X_COORDS[1] + BOX_WIDTH:
		for i in range(len(BOX_Y_COORDS)):
			if BOX_Y_COORDS[i] <= y_coord <= BOX_Y_COORDS[i] + BOX_HEIGHT:
				return len(EMPTY_BOX_DURATIONS.keys()) // 2 + i
	else:
		return None


files = [filename for filename in os.listdir(input_dir) if not filename.startswith('stats') and
         not filename.startswith('samples') and 'combined' not in filename and 'answers' not in filename
         and filename != "movements.csv" and filename != "participants.csv" and filename.endswith('.csv')]
for filename in files:
	durations = dict()
	curr_file = os.path.join(input_dir, filename)

	stats_file = os.path.join(input_dir, 'stats' + '_' + ntpath.basename(curr_file))
	# noinspection PyUnboundLocalVariable
	# if not computing_for_test_data and filename[:UUID_LENGTH] not in valid_users:
	# 	if os.path.isfile(stats_file):
	# 		os.remove(stats_file)
	# 	continue
	if os.path.isfile(stats_file):
		continue
	with open(curr_file, 'r') as csv_file:

		reader = csv.reader(csv_file, delimiter=',')
		prev_question = ''

		# Variables to keep track of box stats
		box_durations = dict(EMPTY_BOX_DURATIONS)
		order_box_opened = []
		opened_box_set = set()
		last_box = None
		startTimestamp = None
		endTimestamp = None

		with open(stats_file, 'w') as output:
			writer = csv.writer(output, delimiter=',')

			for row in reader:
				# tag = get_tag_from_uuid(row[0], tag_to_valid_users)
				# if tag == 'delta':
				# 	break
				# else:
				# 	copyfile(curr_file, os.path.join(input_dir, 'tally_ttb', filename))
				# 	break

				# For data collected before (excluding) 04/26/2017, the order of columns in the movements
				# file is different than the order in files collected later (e.g. g_data).
				# For newer data, change below to curr_row = row
				# curr_row = [row[0], row[3], row[4], row[2], row[1]]
				curr_row = row

				opened_box_num = opened_box_number(float(curr_row[1]), float(curr_row[2]))

				if opened_box_num != last_box:
					# start and end timestamps both cannot be None, otherwise a box is only reached at one time instant,
					# causing no duration can be computed.
					if last_box is not None and endTimestamp is not None and startTimestamp is not None \
							and endTimestamp > startTimestamp:
						box_durations[last_box] += timedelta.total_seconds(endTimestamp - startTimestamp)
						prev_box_count = get_num_opened_boxes(opened_box_set)
						opened_box_set.add(last_box)
						if get_num_opened_boxes(opened_box_set) > prev_box_count:
							order_box_opened.append(last_box + 1)

					last_box = opened_box_num
					if opened_box_num is not None:
						startTimestamp = parser.parse(curr_row[3])
				else:
					endTimestamp = parser.parse(curr_row[3])

				if prev_question == '':
					start = parser.parse(curr_row[3])
					prev_question = curr_row[4]
				elif curr_row[4] != prev_question:
					durations[prev_question] = timedelta.total_seconds(end - start)
					sorted_box_durations = collections.OrderedDict(sorted(box_durations.items()))
					game_id = int(curr_row[4]) - 1

					if not computing_for_test_data:
						# tag = get_tag_from_uuid(curr_row[0], tag_to_valid_users)
						tag = uuid_strategy_map[curr_row[0]]
						if tag != INVALID_FLAG and len(order_box_opened) > 0:
							# print(order_box_opened)
							output_row = [tag, curr_row[0], game_id, row_when_decision_made(order_box_opened),
							              durations[prev_question]] \
							             + list(sorted_box_durations.values()) + set_order_unopened(order_box_opened)
							writer.writerow(output_row)
					else:
						output_row = [curr_row[0], game_id, row_when_decision_made(order_box_opened),
						              durations[prev_question]] \
						             + list(sorted_box_durations.values()) + set_order_unopened(order_box_opened)
						writer.writerow(output_row)

					# For each new question, re-initialize box stats bookkeeping
					box_durations = dict(EMPTY_BOX_DURATIONS)  # Use dict() to copy dictionary
					order_box_opened = []
					opened_box_set = set()
					curr_box = None
					startTimestamp = None
					endTimestamp = None
					prev_question = curr_row[4]
					start = parser.parse(curr_row[3])
				else:
					end = parser.parse(curr_row[3])

			# Compute duration for the last question
			durations[prev_question] = timedelta.total_seconds(end - start)
			sorted_box_durations = collections.OrderedDict(sorted(box_durations.items()))
			game_id = int(curr_row[4])
			if not computing_for_test_data:
				# tag = get_tag_from_uuid(curr_row[0], tag_to_valid_users)
				tag = uuid_strategy_map[curr_row[0]]
				if tag != INVALID_FLAG and len(order_box_opened) > 0:
					print(order_box_opened)
					output_row = [tag, curr_row[0], game_id, row_when_decision_made(order_box_opened),
					              durations[prev_question]] \
					             + list(sorted_box_durations.values()) + set_order_unopened(order_box_opened)
					writer.writerow(output_row)
			else:
				output_row = [curr_row[0],
				              game_id,
				              row_when_decision_made(order_box_opened),
				              durations[prev_question]
				              ] + \
				             list(sorted_box_durations.values()) + \
				             set_order_unopened(order_box_opened)
				writer.writerow(output_row)
