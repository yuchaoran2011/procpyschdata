#!/usr/bin/env bash
input_dir="$1"
output_dir="$2"

mkdir -p ${output_dir}
script_path=$(dirname "$0")

rm -f ${output_dir}/*.txt

for d in $(find "${input_dir}" -maxdepth 1 -mindepth 1 -type d); do
    "${script_path}"/process_csv.py $d -m $d/movements*.csv -a $d/answers*.csv
    "${script_path}"/filter_users.py $d/answers*.csv $d "${output_dir}" -c 80
done
