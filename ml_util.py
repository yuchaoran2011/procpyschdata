import copy
import csv
import sys
import joblib
import numpy as np
import os

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import pyplot as plt

from sklearn import ensemble, preprocessing, metrics, model_selection, tree, decomposition

NUM_FEATURES = 26

original_ordering = [i for i in range(120)]


def strategy_for_user(uuid,
                      class1_name_and_valid_users,
                      class2_name_and_valid_users,
                      class3_name_and_valid_users):
	(class1_name, valid_class1_users) = class1_name_and_valid_users
	(class2_name, valid_class2_users) = class2_name_and_valid_users
	(class3_name, valid_class3_users) = class3_name_and_valid_users
	if uuid in valid_class1_users:
		return class1_name
	elif uuid in valid_class2_users:
		return class2_name
	elif uuid in valid_class3_users:
		return class3_name
	else:
		return 'UNKNOWN'


def cache_data_file(data_file):
	rows = []
	with open(data_file, 'r') as csv_file:
		reader = csv.reader(csv_file, delimiter=',', dialect=csv.excel_tab)
		for row in reader:
			rows.append(row)
	return rows


def get_question_ordering(participant_file):
	uuid_to_ordering = dict()
	with open(participant_file, 'r') as csv_file:
		reader = csv.reader(csv_file, delimiter=',', dialect=csv.excel_tab)
		next(reader)  # Skip header
		for row in reader:
			uuid_to_ordering[row[0]] = eval(row[-1])
	return uuid_to_ordering


def read_user_ans_files(raw_data_dirs, two_levels_deep, answers_str_at_start=True):
	user_ans = dict()
	if two_levels_deep:
		for raw_data_dir in raw_data_dirs:
			sub_dirs = [name for name in os.listdir(raw_data_dir) if os.path.isdir(os.path.join(raw_data_dir, name))]
			for directory in sub_dirs:
				curr_dir = os.path.join(raw_data_dir, directory)
				print(curr_dir)
				answers_files = [name for name in os.listdir(curr_dir) if
				                 name.startswith('answers') and name.endswith('.csv')]
				if len(answers_files) == 0:
					print('No answers file found')
					continue
				# There should be only one answers file (the one that contains data for all users i.e. the unsplit one) in the directory.
				answers_file = answers_files[0]
				ans_file = os.path.join(curr_dir, answers_file)
				with open(ans_file, 'r') as csv_file:
					reader = csv.reader(csv_file, delimiter=',')
					next(reader, None)
					for row in reader:
						user_ans[(row[0], int(row[1]))] = row[3]
	else:
		if answers_str_at_start:
			answers_files = [name for name in os.listdir(raw_data_dirs) if
			                 name.startswith('answers') and name.endswith('.csv')]
		else:
			answers_files = [name for name in os.listdir(raw_data_dirs) if
			                 'answers' in name and name.endswith('.csv')]
		if len(answers_files) == 0:
			return user_ans
		# There should be only one answers file (the one that contains data for all users i.e. the unsplit one) in the directory
		# when there is a file that STARTS with 'answers' in the file name
		if answers_str_at_start:
			answers_file = answers_files[0]
			ans_file = os.path.join(raw_data_dirs, answers_file)
			with open(ans_file, 'r') as csv_file:
				reader = csv.reader(csv_file, delimiter=',')
				next(reader, None)
				for row in reader:
					user_ans[(row[0], int(row[1]))] = row[3]
		else:
			for answers_file in answers_files:
				ans_file = os.path.join(raw_data_dirs, answers_file)
				with open(ans_file, 'r') as csv_file:
					reader = csv.reader(csv_file, delimiter=',')
					next(reader, None)
					for row in reader:
						user_ans[(row[0], int(row[1]))] = row[3]
	return user_ans


def gen_images(training_data_dir, dim,
               ttb_valid_users, delta_valid_users, tally_valid_users,
               ttb_class_name, delta_class_name, tally_class_name,
               strategy_to_label_mapping,
               count_points=True):
	factor = int(1000 / dim)
	uuid_qid_to_image = {}
	files = [filename for filename in os.listdir(training_data_dir) if not filename.startswith('stats') and
	         not filename.startswith('samples') and 'combined' not in filename and filename.endswith('.csv')]
	for file_name in files:
		with open(file_name, 'r') as csv_file:
			reader = csv.reader(csv_file, delimiter=',')
			last_question = '0'
			curr_value = 1
			curr_image = np.zeros((dim, dim, 1), dtype=np.int)
			for row in reader:
				x_coord = int(np.math.floor(float(row[3]) / factor))
				y_coord = int(np.math.floor(float(row[4]) / factor))
				strategy = strategy_for_user(row[0],
				                             (ttb_class_name, ttb_valid_users),
				                             (delta_class_name, delta_valid_users),
				                             (tally_class_name, tally_valid_users))
				if strategy == 'UNKNOWN':
					continue
				label = strategy_to_label_mapping[strategy]
				if row[1] == last_question:
					if count_points:
						curr_image[x_coord, y_coord, 0] = curr_image.item((x_coord, y_coord, 0)) + 1
					else:
						curr_image[x_coord, y_coord, 0] = curr_image.item((x_coord, y_coord, 0)) + curr_value
						curr_value += 1
				else:
					uuid_qid_to_image[(row[0], row[1])] = (curr_image, label)
					# images = np.append(images, curr_image, 2)
					last_question = row[1]
					curr_image = np.zeros((dim, dim, 1), dtype=np.int)
	# images.shape
	# (100, 100, 1639)
	# print(images.shape)
	return uuid_qid_to_image


def gen_empty_five_classes(num_features):
	data_map = {}
	label_map = {}
	for i in range(5):
		data_map[i] = np.ndarray((0, num_features))
		label_map[i] = []
	return data_map, label_map


def plot_pca(data, labels, filter_threshold):
	fig = plt.figure(1, figsize=(8, 6))
	ax = Axes3D(fig, elev=-150, azim=110)
	pca = decomposition.PCA(n_components=3)
	data = pca.fit_transform(data)

	to_delete = []
	for i in range(np.shape(data)[0]):
		if np.abs(data[i, 0]) > filter_threshold or np.abs(data[i, 1]) > filter_threshold or np.abs(
				data[i, 2]) > filter_threshold:
			to_delete.append(i)
	print('Outliers to delete: ', len(to_delete))
	data = np.delete(data, to_delete, 0)
	labels = np.delete(labels, to_delete, 0)

	ax.scatter(data[:, 0], data[:, 1], data[:, 2], c=labels, cmap=plt.cm.Paired)
	ax.set_title('First three PCA directions')
	ax.set_xlabel('1st eigenvector')
	ax.w_xaxis.set_ticklabels([])
	ax.set_ylabel('2nd eigenvector')
	ax.w_yaxis.set_ticklabels([])
	ax.set_zlabel('3rd eigenvector')
	ax.w_zaxis.set_ticklabels([])
	plt.show()
	print('PCA explained variance ratios: ', pca.explained_variance_ratio_)


def compute_hint(row_num, q_num, stimuli_rows):
	data_row = stimuli_rows[q_num]
	for i in range(row_num):
		if data_row[i] != data_row[i + 5]:
			return 1
	return 0


def gen_data_label_dicts(num_features,
                         stats_combined_file,
                         strategy_to_label_mapping,
                         leading_features_to_discard,
                         stimuli_rows,
                         user_answers,
                         num_questions,
                         class1_name,
                         class2_name,
                         class3_name,
                         add_hint=True):
	data_map, label_map = gen_empty_five_classes(num_features)
	with open(stats_combined_file, 'r') as csv_file:
		reader = csv.reader(csv_file, delimiter=',')
		for row in reader:
			if row[3] == '-1':
				continue
			label = strategy_to_label_mapping[row[0]]

			row_decision_made = int(row[3])
			q_num = int(row[2])
			q_num_lookup = q_num % num_questions
			user_id_and_q_num = (row[1], q_num)
			if user_id_and_q_num not in user_answers:
				continue
			user_answer = user_answers[user_id_and_q_num]
			box_open_proportion = len([x for x in row[15:25] if x != '0']) / 10
			strategy_matches = generate_strategy_matches(stimuli_rows, user_answer, q_num_lookup)
			if add_hint:
				if label == strategy_to_label_mapping[class3_name]:
					hint = 1
				else:
					row_decision_made = int(row[3])
					if label == strategy_to_label_mapping[class1_name]:
						hint = compute_hint(row_decision_made, q_num_lookup, stimuli_rows)
					else:
						hint = compute_hint(row_decision_made, q_num_lookup, stimuli_rows)
				curr_data = np.asarray(
					[hint, box_open_proportion] + row[leading_features_to_discard:] + strategy_matches,
					dtype=float).reshape(1, num_features)
			else:
				curr_data = np.asarray([box_open_proportion] + row[leading_features_to_discard:] + strategy_matches,
				                       dtype=float).reshape(1, num_features)
			data_map[row_decision_made] = np.append(data_map[row_decision_made], curr_data, 0)
			label_map[row_decision_made].append(label)
	return data_map, label_map


def generate_strategy_matches(stimuli_rows, user_answer, q_num_lookup):
	strategy_matches = []
	ttb_true_answer = stimuli_rows[q_num_lookup][-3]
	tally_true_answer = stimuli_rows[q_num_lookup][-2]
	delta_true_answer = stimuli_rows[q_num_lookup][-1]
	if user_answer == ttb_true_answer:
		strategy_matches.append(1)
	else:
		strategy_matches.append(0)
	if user_answer == delta_true_answer:
		strategy_matches.append(1)
	else:
		strategy_matches.append(0)
	if tally_true_answer == 'N' or user_answer == tally_true_answer:
		strategy_matches.append(1)
	else:
		strategy_matches.append(0)
	return strategy_matches


def gen_data(num_features,
             file_name,
             leading_features_to_discard,
             stimuli_rows,
             user_answers,
             num_questions,
             strategy_to_label_mapping,
             class1_name,
             class2_name,
             class3_name,
             question_ordering=None,
             add_hint=False):
	raw_labels = []
	raw_data = np.ndarray((0, num_features))

	# uuid_to_answers is an array where each row has 4 columns
	# uuid, question number, user answer, true answer
	# uuid_to_answers = np.ndarray((0, 4))
	uuid_to_answers = dict()

	# user_to_datapoints is a map from UUID to a list of data points for each user
	# Data for each question is a data point. A key can therefore map to a list of max length NUM_QUESTIONS.
	uuid_to_datapoints = dict()
	with open(file_name, 'r') as csv_file:
		reader = csv.reader(csv_file, delimiter=',')
		for row in reader:
			label = strategy_to_label_mapping[row[0].lower()]

			if question_ordering is None:
				q_num = int(row[2])
			else:
				q_num = question_ordering[row[1]][int(row[2])]
			q_num_lookup = q_num % num_questions
			user_id_and_q_num = (row[1], q_num)
			if user_id_and_q_num not in user_answers:
				continue
			user_answer = user_answers[user_id_and_q_num]

			if row[0] == class1_name:
				uuid_to_answers[(row[1], q_num)] = (class1_name, user_answer, stimuli_rows[q_num_lookup][-3])
			elif row[0] == class2_name:
				uuid_to_answers[(row[1], q_num)] = (class2_name, user_answer, stimuli_rows[q_num_lookup][-2])
			else:
				uuid_to_answers[(row[1], q_num)] = (class3_name, user_answer, stimuli_rows[q_num_lookup][-1])

			box_open_proportion = len([x for x in row[15:25] if x != '0']) / 10
			strategy_matches = generate_strategy_matches(stimuli_rows, user_answer, q_num_lookup)
			num_row_data = list(map(lambda x: float(x), row[leading_features_to_discard:]))
			if add_hint:
				if label == strategy_to_label_mapping[class3_name]:
					hint = 1
				else:
					row_decision_made = int(row[3])
					q_num_lookup = q_num % num_questions
					if label == strategy_to_label_mapping[class1_name]:
						hint = compute_hint(row_decision_made, q_num_lookup, stimuli_rows)
					else:
						hint = compute_hint(row_decision_made, q_num_lookup, stimuli_rows)
				curr_data = np.asarray([hint, box_open_proportion] + num_row_data + strategy_matches,
				                       dtype=float).reshape(1, num_features)
			else:
				curr_data = np.asarray([box_open_proportion] + num_row_data + strategy_matches, dtype=float).reshape(1,
				                                                                                                     num_features)

			datapoint = np.append(curr_data, [label])
			if row[1] in uuid_to_datapoints:
				uuid_to_datapoints[row[1]].append(datapoint)
			else:
				uuid_to_datapoints[row[1]] = [datapoint]
			raw_data = np.append(raw_data, curr_data, 0)
			raw_labels.append(label)
	for uuid in uuid_to_datapoints:
		uuid_to_datapoints[uuid] = uuid_to_datapoints[uuid][:num_questions]
	return raw_data, raw_labels, uuid_to_answers, uuid_to_datapoints


def split_five_classes(all_data, all_labels):
	num_features = all_data.shape[1] - 1
	data_map, label_map = gen_empty_five_classes(num_features)
	for i in range(all_data.shape[0]):
		row_decision_made = int(all_data[(i, 0)])
		curr_data = np.ndarray((1, num_features))
		for j in range(num_features):
			curr_data[(0, j)] = all_data[i, j + 1]
		data_map[row_decision_made] = np.append(data_map[row_decision_made], curr_data, 0)
		label_map[row_decision_made].append(all_labels[i])
	return data_map, label_map


def add_row_decision_made_to_images(uuid_qid_to_image, dim):
	new_total = dim * dim + 1
	reshaped_images = np.ndarray((new_total, 0))
	image_labels = []
	with open('stats_combined.csv', 'r') as csv_file:
		reader = csv.reader(csv_file, delimiter=',')
		for row in reader:
			uuid = row[1]
			qid = str(int(row[2]) + 1)
			curr_image, label = uuid_qid_to_image[(uuid, qid)]
			new_curr_image = np.zeros((new_total, 1), dtype=np.int)
			row_decision_made = int(row[3])
			if row_decision_made == -1:
				continue
			new_curr_image[0, 0] = row_decision_made
			for i in range(new_total - 1):
				x_coord = i // dim
				y_coord = i % dim
				new_curr_image[i + 1, 0] = curr_image.item((x_coord, y_coord, 0))
			reshaped_images = np.append(reshaped_images, new_curr_image, 1)
			image_labels.append(label)
	return reshaped_images, image_labels


def random_permute_data_labels(nonrandom_data, nonrandom_labels, normalize=False):
	# Randomly permute the order of data points
	# by indexing labels and data arrays with an order array
	random_order = np.random.permutation(np.shape(nonrandom_labels)[0])
	nonrandom_labels = np.asarray(nonrandom_labels)
	random_labels = nonrandom_labels[random_order]
	random_data = nonrandom_data[random_order]
	if normalize:
		random_data = preprocessing.normalize(random_data, axis=0)
	return random_data, random_labels


def random_permute_images_labels(nonrandom_images, nonrandom_labels):
	return random_permute_data_labels(nonrandom_images, nonrandom_labels, nonrandom_images.shape[2])


def random_permute_data_label_dicts(data_dict, label_dict):
	for i in range(5):
		random_data, random_labels = random_permute_data_labels(data_dict[i], label_dict[i], data_dict[i].shape[1])
		data_dict[i] = random_data
		label_dict[i] = random_labels


def partition_data(ml_data, ml_labels):
	train_size = int(len(ml_labels) * .8)
	training_data, test_data, training_labels, test_labels = model_selection.train_test_split(ml_data, ml_labels,
	                                                                                          train_size=train_size,
	                                                                                          shuffle=True)
	return training_data, test_data, training_labels, test_labels


# This method partitions the input dataset into training and heldout datasets
# where the heldout dataset consists of 42 subjects who have full 40 data points
# for a total records of 1680. Each tag (ttb etc) has 14 subjects.
def partition_full_heldout(uuid_to_datapoints, uuid_to_answers,
                           num_questions,
                           ttb_class_name, delta_class_name, tally_class_name,
                           ttb_test_num, delta_test_num, tally_test_num,
                           strategy_to_label_mapping,
                           train_ttb_num=sys.maxsize, train_delta_num=sys.maxsize, train_tally_num=sys.maxsize):
	heldout_uuid_ttb = set()
	heldout_uuid_delta = set()
	heldout_uuid_tally = set()
	train_uuid_ttb = set()
	train_uuid_delta = set()
	train_uuid_tally = set()

	for key in sorted(uuid_to_datapoints.keys()):
		tag = uuid_to_datapoints[key][0][-1]
		print('# data points', len(uuid_to_datapoints[key]))
		# if len(uuid_to_datapoints[key]) != num_questions:
		# 	continue
		if len(heldout_uuid_ttb) == ttb_test_num and len(heldout_uuid_tally) == tally_test_num and len(
				heldout_uuid_delta) == delta_test_num:
			break
		if tag == strategy_to_label_mapping[ttb_class_name] and len(heldout_uuid_ttb) < ttb_test_num:
			heldout_uuid_ttb.add(key)
		elif tag == strategy_to_label_mapping[delta_class_name] and len(heldout_uuid_delta) < delta_test_num:
			heldout_uuid_delta.add(key)
		elif tag == strategy_to_label_mapping[tally_class_name] and len(heldout_uuid_tally) < tally_test_num:
			heldout_uuid_tally.add(key)
		else:
			pass
	print('test tally:', len(heldout_uuid_tally))
	print('test ttb:', len(heldout_uuid_ttb))
	print('test delta:', len(heldout_uuid_delta))
	heldout_uuids = heldout_uuid_ttb.union(heldout_uuid_tally).union(heldout_uuid_delta)

	for key in uuid_to_datapoints.keys():
		if key not in heldout_uuids:
			tag = uuid_to_datapoints[key][0][-1]
			if len(train_uuid_ttb) == train_ttb_num and len(train_uuid_tally) == train_tally_num and len(
					train_uuid_delta) == train_delta_num:
				break
			if tag == strategy_to_label_mapping[ttb_class_name] and len(train_uuid_ttb) < train_ttb_num:
				train_uuid_ttb.add(key)
			elif tag == strategy_to_label_mapping[delta_class_name] and len(train_uuid_delta) < train_delta_num:
				train_uuid_delta.add(key)
			elif tag == strategy_to_label_mapping[tally_class_name] and len(train_uuid_tally) < train_tally_num:
				train_uuid_tally.add(key)
			else:
				pass
	print('train tally:', len(train_uuid_tally))
	print('train ttb:', len(train_uuid_ttb))
	print('train delta:', len(train_uuid_delta))
	train_uuids = train_uuid_ttb.union(train_uuid_tally).union(train_uuid_delta)

	test_data = np.ndarray((0, NUM_FEATURES))
	training_data = np.ndarray((0, NUM_FEATURES))
	test_labels = []
	training_labels = []

	for key in heldout_uuids:
		if len(uuid_to_datapoints[key]) == num_questions:
			for datapoint in uuid_to_datapoints[key]:
				test_data = np.append(test_data, datapoint[:-1].reshape(1, NUM_FEATURES), 0)
				test_labels.append(datapoint[-1])
	for key in train_uuids:
		for datapoint in uuid_to_datapoints[key]:
			training_data = np.append(training_data, datapoint[:-1].reshape(1, NUM_FEATURES), 0)
			training_labels.append(datapoint[-1])

	# with open('/Users/cyu/Documents/heldout30.csv', 'w') as output:
	# 	writer = csv.writer(output, delimiter=',')
	# 	for key in heldout_uuids:
	# 		q_num = 0
	# 		for datapoint in uuid_to_datapoints[key]:
	# 			uuid = key
	# 			duration = datapoint[1]
	# 			label = uuid_to_answers[(key, q_num)][0]
	# 			user_ans = uuid_to_answers[(key, q_num)][1]
	# 			corr_ans = uuid_to_answers[(key, q_num)][2]
	# 			row = [label, uuid, q_num, duration, user_ans, corr_ans]
	# 			writer.writerow(row)
	# 			q_num += 1

	return training_data, test_data, training_labels, test_labels


def train(classifier, X_train, y_train, tuned_params=False):
	clf = copy.deepcopy(classifier)
	clf.fit(X_train, y_train)
	if tuned_params:
		print("Best parameters: {}.".format(str(clf.best_params_)))
	return clf


def predict_and_print_metrics(classifier, X_test, y_test, num_questions):
	predicted = classifier.predict(X_test)
	# probs = clf.predict_proba(X_test)
	# np.savetxt('/Users/cyu/probs.txt', probs, delimiter=',', newline='\n')

	# order_tuples is a list of tuples, where each tuple is the
	# (correct_answer, correctness_rate) for each test subject
	order_tuples = []
	for i in range(len(y_test) // num_questions):
		# print(predicted[num_questions*i:num_questions*(i+1)])
		# print(y_test[num_questions*i:num_questions*(i+1)])
		count = 0
		for idx in range(num_questions * i, num_questions * (i + 1)):
			if predicted[idx] == y_test[idx]:
				count += 1
		correctness = count / num_questions
		order_tuples.append((y_test[num_questions * i], correctness))
	# print('correctness for subject ' + str(i) + ': ' + str(correctness))

	# Produce the sorted indices to be used for plotting
	# https://stackoverflow.com/questions/7851077/how-to-return-index-of-a-sorted-list
	order = sorted(range(len(order_tuples)), key=lambda k: (order_tuples[k][0], order_tuples[k][1]))

	print("Confusion matrix:\n%s" % metrics.confusion_matrix(y_test, predicted))
	print('Accuracy score: %.4f' % metrics.accuracy_score(y_test, predicted, normalize=True))

	accuracy_score = metrics.accuracy_score(y_test, predicted, normalize=False)
	return len(y_test), accuracy_score, classifier, predicted, order


def train_predict_and_print_metrics(classifier,
                                    X_train, X_test,
                                    y_train, y_test,
                                    num_questions,
                                    tuned_params=False):
	clf = train(classifier, X_train, y_train, tuned_params)
	return predict_and_print_metrics(clf, X_test, y_test, num_questions)


def five_class_train_predict(data_dict, label_dict, classifier, num_questions, tuned_params=False, normalize=False):
	total_test_samples = 0
	total_correct_samples = 0
	classifiers = []
	for i in range(5):
		data_i = data_dict[i]
		labels_i = label_dict[i]
		if normalize:
			data_i = preprocessing.normalize(data_i, axis=0)
		a, b, c, d = partition_data(data_i, labels_i)
		num_test_samples, num_correct_samples, clf, _, _ = train_predict_and_print_metrics(classifier,
		                                                                                   a, b, c, d,
		                                                                                   num_questions,
		                                                                                   tuned_params)
		total_test_samples += num_test_samples
		total_correct_samples += num_correct_samples
		classifiers.append(clf)
	print(total_correct_samples)
	print(total_test_samples)
	print('Overall accuracy: %.4f' % (total_correct_samples / total_test_samples))
	return classifiers


def adb_dt_count_points(training_data_dir, dim, valid_ttb_users, valid_delta_users, valid_tally_users):
	uuid_qid_to_image_dict = gen_images(training_data_dir, dim, valid_ttb_users, valid_delta_users, valid_tally_users)
	images, labels = add_row_decision_made_to_images(uuid_qid_to_image_dict, NUM_FEATURES)
	data, labels = random_permute_data_labels(images, labels, images.shape[1])
	data_dict, label_dict = split_five_classes(data, labels)
	clf = ensemble.AdaBoostClassifier(tree.DecisionTreeClassifier(), n_estimators=50)
	return five_class_train_predict(data_dict, label_dict, clf)


def two_labels_five_classes(classifier,
                            num_features,
                            num_questions,
                            training_data_dir,
                            stimuli_rows,
                            strategy_to_label_mapping,
                            user_answers):
	data_dicts, labels_dicts = gen_data_label_dicts(num_features=num_features,
	                                                num_questions=num_questions,
	                                                leading_features_to_discard=4,
	                                                stats_combined_file=os.path.join(training_data_dir,
	                                                                                 'stats_combined.csv'),
	                                                stimuli_rows=stimuli_rows,
	                                                class1_name='ttb',
	                                                class2_name='delta',
	                                                class3_name='tally',
	                                                strategy_to_label_mapping=strategy_to_label_mapping,
	                                                user_answers=user_answers)
	cue_clfs = five_class_train_predict(data_dicts, labels_dicts, classifier, False, False)
	for i in range(5):
		cue_clf = os.path.join(training_data_dir, 'clf_cue' + str(i) + '.pkl')
		joblib.dump(cue_clfs[i], cue_clf)
	return cue_clfs


def five_class_predict(test_data, test_labels, cue_clfs):
	labels = [[], [], [], [], []]
	results = [[], [], [], [], []]
	for i in range(np.shape(test_data)[0]):
		# Reshape your data either using array.reshape(-1, 1) if your data has a single feature
		# or array.reshape(1, -1) if it contains a single sample.
		data = test_data[i, :].reshape(1, -1)
		hint = int(data[0, 0])
		results[hint].append(cue_clfs[hint].predict(data))
		labels[hint].append(test_labels[i])
	for i in range(5):
		if len(labels[i]) > 0:
			print(len([m for m, n in zip(results[i], labels[i]) if m == n]) / len(labels[i]))


def get_outcome(uuid, q_num, true_answers, test_data_dir):
	with open(os.path.join(test_data_dir, uuid + '_answers.csv'), 'r') as csv_file:
		reader = csv.reader(csv_file, delimiter=',')
		csv_list = list(reader)
		user_answer = csv_list[q_num][2]
		true_answer = true_answers[q_num]
		return 1 if user_answer == true_answer else 0


def partition_uuid_to_datapoints(uuid_to_datapoints, class1_test_size, class2_test_size, class3_test_size,
                                 class1_name, class2_name, class3_name, strategy_to_label_mapping):
	test_datapoints = dict()
	count1, count2, count3 = 0, 0, 0
	num_features = 0
	for uuid in uuid_to_datapoints:
		a_datapoint = uuid_to_datapoints[uuid][0]
		if num_features == 0:
			num_features = np.size(a_datapoint) - 1
		if count1 < class1_test_size and a_datapoint[-1] == strategy_to_label_mapping[class1_name]:
			test_datapoints[uuid] = uuid_to_datapoints[uuid]
			count1 += 1
		elif count2 < class2_test_size and a_datapoint[-1] == strategy_to_label_mapping[class2_name]:
			test_datapoints[uuid] = uuid_to_datapoints[uuid]
			count2 += 1
		elif count3 < class3_test_size and a_datapoint[-1] == strategy_to_label_mapping[class3_name]:
			test_datapoints[uuid] = uuid_to_datapoints[uuid]
			count3 += 1
		elif count1 < class1_test_size or count2 < class2_test_size or count3 < class3_test_size:
			continue
		else:
			break

	for uuid in test_datapoints:
		del uuid_to_datapoints[uuid]

	train_ttb_counts = 0
	train_delta_counts = 0
	train_tally_counts = 0
	for elem in uuid_to_datapoints.keys():
		a_datapoint = uuid_to_datapoints[elem][0]
		if a_datapoint[-1] == strategy_to_label_mapping[class1_name]:
			train_ttb_counts += 1
		elif a_datapoint[-1] == strategy_to_label_mapping[class2_name]:
			train_delta_counts += 1
		else:
			train_tally_counts += 1
	print(f'ttb: {train_ttb_counts}, delta: {train_delta_counts}, tally: {train_tally_counts}\n')

	X_test = np.ndarray((0, num_features))
	y_test = []
	X_train = np.ndarray((0, num_features))
	y_train = []

	for datapoints in uuid_to_datapoints.values():
		for datapoint in datapoints:
			y_train.append(datapoint[-1])
			X_train = np.append(X_train, datapoint[:-1].reshape(1, num_features), 0)
	for datapoints in test_datapoints.values():
		for datapoint in datapoints:
			y_test.append(datapoint[-1])
			X_test = np.append(X_test, datapoint[:-1].reshape(1, num_features), 0)
	return X_train, y_train, X_test, y_test


def split_test_dataset_into_five_classes(X_test, y_test, stimuli_rows, num_features, num_questions):
	classes = [np.ndarray((0, num_features)) for x in range(5)]
	class_y = [[] for x in range(5)]
	for i in range(X_test.shape[0]):
		q_num = i % num_questions
		if int(y_test[i]) == 0:
			class_label = int(stimuli_rows[q_num][-6])
		elif int(y_test[i]) == 1:
			class_label = int(stimuli_rows[q_num][-5])
		else:
			class_label = 5
		classes[class_label - 1] = np.append(classes[class_label - 1], X_test[i].reshape(1, num_features), 0)
		class_y[class_label - 1].append(y_test[i])
	return classes, class_y


def gen_test_data(num_features,
                  test_data_file,
                  leading_features_to_discard,
                  test_stimuli_rows,
                  user_answers,
                  num_questions,
                  add_hint=False):
	test_data = np.ndarray((0, num_features))

	# if gen_outcomes:
	# 	with open(test_data_file, 'r') as csv_file:
	# 		csv_list = list(csv.reader(csv_file, delimiter=','))
	# 		true_answers = [ans[-1] for ans in csv_list]
	# 	outcomes = []

	with open(test_data_file, 'r') as csv_file:
		reader = csv.reader(csv_file, delimiter=',')
		# curr_user = None
		# curr_outcomes = []
		for row in reader:
			q_num = int(row[2])
			q_num_lookup = q_num % num_questions
			user_id_and_q_num = (row[1], q_num)
			if user_id_and_q_num not in user_answers:
				continue
			user_answer = user_answers[user_id_and_q_num]
			box_open_proportion = len([x for x in row[15:25] if x != '0']) / 10
			strategy_matches = generate_strategy_matches(test_stimuli_rows, user_answer, q_num_lookup)

			num_row_data = list(map(lambda x: float(x), row[leading_features_to_discard:]))

			if add_hint:
				row_decision_made = int(row[3])
				q_num_lookup = q_num % num_questions
				hint = compute_hint(row_decision_made, q_num_lookup, test_stimuli_rows)
				curr_data = np.asarray([hint, box_open_proportion] + num_row_data + strategy_matches,
				                       dtype=float).reshape(1, num_features)
			else:
				curr_data = [box_open_proportion] + np.asarray(num_row_data + strategy_matches, dtype=float).reshape(1,
				                                                                                                     num_features)
			# if gen_outcomes:
			# 	curr_outcome = get_outcome(row[0], int(row[1]), true_answers, test_data_dir)
			# 	if row[0] != curr_user:
			# 		if len(curr_outcomes) == num_questions:
			# 			outcomes.append(curr_outcomes)
			# 		curr_user = row[0]
			# 		curr_outcomes = [curr_outcome]
			# 	else:
			# 		curr_outcomes.append(curr_outcome)

			test_data = np.append(test_data, curr_data, 0)
	# if gen_outcomes:
	# 	if len(curr_outcomes) == num_questions:
	# 		outcomes.append(curr_outcomes)
	# 	outcomes = np.asarray(outcomes, np.int)
	# 	return test_data, outcomes
	# else:
	return test_data
