#!/usr/local/bin/python3
# ./join_ans_durations.py ../result
import csv

from dateutil import parser
from datetime import timedelta
import ntpath

from ml_util import read_user_ans_files
from util import *

arg_parser.add_argument('input_dir')
arg_parser.add_argument('raw_data_dir')
arg_parser.add_argument('data_file')
args = arg_parser.parse_args()
input_dir = args.input_dir
raw_data_dir = args.raw_data_dir
data_file = args.data_file

correct_ans = dict()

user_ans = read_user_ans_files([raw_data_dir])


def get_ans_file_name(experiment_date):
	# Get the containing directory of the answers file
	year = str(experiment_date.year)[2:]
	month = str(experiment_date.month)
	month = '0' + month if len(month) == 1 else month
	day = str(experiment_date.day)
	day = '0' + day if len(day) == 1 else day
	return os.path.join(raw_data_dir, year + month + day, 'answers.csv')


with open(data_file, 'r') as csv_file:
	reader = csv.reader(csv_file, delimiter=',')
	q_num = 0
	for row in reader:
		correct_ans[q_num] = row[-1]
		q_num += 1


files = [filename for filename in os.listdir(input_dir) if not filename.startswith('stats') and
	     not filename.startswith('samples') and 'combined' not in filename and 'answers' not in filename and filename.endswith('.csv')]

for filename in files:
	durations = dict()
	curr_file = os.path.join(input_dir, filename)

	stats_file = os.path.join(input_dir, 'stats' + '_' + ntpath.basename(curr_file))

	with open(curr_file, 'r') as csv_file:

		reader = csv.reader(csv_file, delimiter=',')
		prev_question = ''

		with open(stats_file, 'w') as output:
			writer = csv.writer(output, delimiter=',')

			for row in reader:
				if prev_question == '':
					prev_id = row[0]
					start = parser.parse(row[3])
					prev_question = row[4]
				elif row[4] != prev_question:
					corr_answer = correct_ans[int(row[4])]
					try:
						user_answer = user_ans[(prev_id, row[4])]
					except KeyError:
						pass
					durations[prev_question] = timedelta.total_seconds(end - start)
					game_id = int(row[4]) - 1
					output_row = [game_id, corr_answer, user_answer, durations[prev_question]]
					writer.writerow(output_row)

					# For each new question, re-initialize box stats bookkeeping
					prev_question = row[4]
					prev_id = row[0]
					start = parser.parse(row[3])
				else:
					end = parser.parse(row[3])

			# Compute duration for the last question
			durations[prev_question] = timedelta.total_seconds(end - start)
			game_id = int(row[4])
			corr_answer = correct_ans[game_id]
			try:
				user_answer = user_ans[(prev_id, row[4])]
			except KeyError:
				pass
			output_row = [game_id, corr_answer, user_answer, durations[prev_question]]
			writer.writerow(output_row)
