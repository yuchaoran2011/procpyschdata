#!/usr/local/bin/python3
# ./filter_users.py answers.csv input_dir output_dir --correctness 80

import csv

from util import *

args = parse_args_filter_users_script()
answers_file = args.answers_file
input_dir = args.input_dir
output_dir = args.output_dir
test_data = args.test_data
if not test_data:
	# corr = int(args.correctness)
	correctness_threshold = 0
	# if corr < 100:
	# 	correctness_threshold = corr / 100.0
	# else:
	# 	correctness_threshold = 1

users = dict()
valid_users = []
min_question_answered = 30

ttb = []
tally = []
delta = []
valid_ttb = []
valid_tally = []
valid_delta = []

if not test_data:
	print('Validation conditions used: correctness_threshold = {}, min_question_answered = {}.'.format(str(
		correctness_threshold), str(min_question_answered)))

	with open(answers_file, 'r') as csv_file:
		reader = csv.reader(csv_file, delimiter=',')
		next(reader)
		for row in reader:
			result = 1 if row[2] == row[3] else 0
			key = row[0]
			if key in users:
				correct, count = users[key]
				correct += result
				count += 1
				users[key] = (correct, count)
			else:
				if row[-1] == 'ttb':
					ttb.append(row[0])
				elif row[-1] == 'tally':
					tally.append(row[0])
				else:
					delta.append(row[0])
				users[key] = (result, 1)

	for k, v in users.items():
		if v[1] >= min_question_answered and v[0] / v[1] >= correctness_threshold:
			correctness_rate = v[0] / v[1]
			print('Test subject', k, correctness_rate)
			curr_user = k
			if curr_user in ttb:
				valid_ttb.append(curr_user)
			elif curr_user in tally:
				valid_tally.append(curr_user)
			else:
				valid_delta.append(curr_user)
	valid_users = valid_ttb + valid_tally + valid_delta
else:
	answers_files = [filename for filename in os.listdir(input_dir) if filename.endswith('_answers.csv')]
	for individual_file in answers_files:
		with open(os.path.join(input_dir, individual_file), 'r') as csv_file:
			reader = csv.reader(csv_file, delimiter=',')
			row_count = sum(1 for row in reader)
			if row_count == NUM_QUESTIONS:
				valid_users.append(individual_file[0:UUID_LENGTH])

print('Number of valid users: {}'.format(str(len(valid_users))))

create_directory_if_not_exists(output_dir)

if not test_data:
	print('Total number of users: ' + str(len(users.keys())))
	print('Among {} TTB users, {} are valid.'.format(str(len(ttb)), str(len(valid_ttb))))
	print('Among {} Tally users, {} are valid.'.format(str(len(tally)), str(len(valid_tally))))
	print('Among {} Delta users, {} are valid.'.format(str(len(delta)), str(len(valid_delta))))

	ttb_file = open(os.path.join(output_dir, 'valid_ttb_users.txt'), 'a')
	for row in valid_ttb:
		ttb_file.write("%s\n" % row)

	tally_file = open(os.path.join(output_dir, 'valid_tally_users.txt'), 'a')
	for row in valid_tally:
		tally_file.write("%s\n" % row)

	delta_file = open(os.path.join(output_dir, 'valid_delta_users.txt'), 'a')
	for row in valid_delta:
		delta_file.write("%s\n" % row)

	all_users_file = open(os.path.join(output_dir, 'all_users.txt'), 'a')
	for row in users.keys():
		all_users_file.write("%s\n" % row)

	files = [filename for filename in os.listdir(input_dir) if filename.endswith('.csv')]
	valid_users_file = open(os.path.join(output_dir, 'valid_users.txt'), 'a')
	for row in valid_users:
		valid_users_file.write("%s\n" % row)
		filtered_files = [csv_file for csv_file in files if csv_file.startswith(row)]
		for filtered_file in filtered_files:
			os.rename(os.path.join(input_dir, filtered_file), os.path.join(output_dir, filtered_file))
