import argparse
import os

AMBIGUITY_THRESHOLD_PROB = .40
INVALID_FLAG = 'invalid'
SAMPLES_PER_QUESTION = 100

DURATION_PER_QUESTION_IN_SECS = 20
UUID_LENGTH = 36

arg_parser = argparse.ArgumentParser()


def create_directory_if_not_exists(dir):
	if not os.path.exists(dir):
		os.makedirs(dir)


def get_users_in_file(path, file_name):
	data_file = open(os.path.join(path, file_name))
	results = set(line.strip() for line in data_file)
	data_file.close()
	return results


def parse_args_process_csv_script():
	arg_parser.add_argument('output_dir')
	arg_parser.add_argument('-m', '--movements_file')
	arg_parser.add_argument('-a', '--answers_file')
	return arg_parser.parse_args()


def parse_args_compute_scores_script():
	arg_parser.add_argument('input_dir')
	arg_parser.add_argument('-n', '--num_questions', help='number of questions for each subject')
	return arg_parser.parse_args()


def parse_args_filter_users_script():
	arg_parser.add_argument('answers_file')
	arg_parser.add_argument('input_dir')
	arg_parser.add_argument('output_dir')
	arg_parser.add_argument('-t', '--test_data', action='store_true')
	arg_parser.add_argument('-c', '--correctness', help='correctness threshold')
	return arg_parser.parse_args()


def parse_args_compute_stats_script():
	arg_parser.add_argument('input_dir')
	arg_parser.add_argument('-t', '--test_data', action='store_true')
	return arg_parser.parse_args()


def parse_args_run_ml_script():
	arg_parser.add_argument('--training_data_dir')
	arg_parser.add_argument('--ttb_data_file')
	arg_parser.add_argument('--delta_data_file')
	arg_parser.add_argument('--tally_data_file')
	arg_parser.add_argument('--test_ttb_data_file')
	arg_parser.add_argument('--test_tally_data_file')
	arg_parser.add_argument('--raw_data_dir')
	arg_parser.add_argument('-t', '--run_on_test_data', action='store_true')
	arg_parser.add_argument('--test_data_dir')
	arg_parser.add_argument('--test_data_file')
	arg_parser.add_argument('-g', '--gen_outcomes', action='store_true')
	return arg_parser.parse_args()


def get_tag_from_uuid(uuid, tags_to_valid_users):
	for key in tags_to_valid_users:
		if uuid in tags_to_valid_users[key]:
			return key
	return INVALID_FLAG
