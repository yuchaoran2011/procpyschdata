#!/usr/local/bin/python3
# ./process_csv.py output_dir -m movements.csv -a answers.csv
import csv
import ntpath

from util import *

user_movements = dict()
user_answers = dict()

args = parse_args_process_csv_script()
dest = args.output_dir
movements_file = args.movements_file
answers_file = args.answers_file

create_directory_if_not_exists(dest)

reorder_columns = False

if movements_file:
	with open(movements_file, 'r') as csv_file:
		reader = csv.reader(csv_file, delimiter=',')
		header = next(reader)

		if header[0].lower() == 'uuid' and header[-1] != 'game_id':
			# Make sure all rows are ordered as: uuid, x_coord, y_coord, timestamp, game_id
			reorder_columns = True

		for row in reader:
			reordered_row = []
			if reorder_columns:
				reordered_row = [row[0], row[3], row[4], row[2], row[1]]
			else:
				reordered_row = row[:]
			if reordered_row[0] in user_movements:
				user_movements[reordered_row[0]].append(reordered_row)
			else:
				user_movements[reordered_row[0]] = [reordered_row]
	for key in user_movements:
		curr_user = user_movements[key]
		mvmt_file = os.path.join(dest, key + '_movements.csv')
		if os.path.exists(mvmt_file):
			continue
		with open(mvmt_file, 'w') as output:
			writer = csv.writer(output, delimiter=',')
			sorted_user_data = sorted(curr_user, key=lambda x: x[3])
			for row in sorted_user_data:
				writer.writerow(row)

if answers_file:
	with open(answers_file, 'r') as csv_file:
		reader = csv.reader(csv_file, delimiter=',')
		header = next(reader)
		for row in reader:
			if row[0] in user_answers:
				user_answers[row[0]].append(row)
			else:
				user_answers[row[0]] = [row]
	for key in user_answers:
		curr_user = user_answers[key]
		with open(os.path.join(dest, key + '_' + ntpath.basename(answers_file)), 'w') as output:
			writer = csv.writer(output, delimiter=',')
			sorted_user_data = sorted(curr_user, key=lambda x: x[3])
			for row in sorted_user_data:
				writer.writerow(row)
